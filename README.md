# Pit Sign Sketch #

This repository is to store the code and supporting documentation for the 2016 Raptors Pit sign.  The sign lighting is driven by an Arduino Uno, and a few Radio Shack Tri Color LED strips.  Also there are some single RED led's that light up the nostril of the sign.

The sign is pretty simple.

The RS LED Strips are 12v, plug into Vin = RED line, Grd = Black line, A0(signal) = Green line.

### Who do I talk to? ###

* Initial code was created by Colin Riker and Mr. Riker