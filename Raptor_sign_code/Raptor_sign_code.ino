/*
 * ********************************************************************************
 * Program: Raptor_sign_code.ino
 * Date: 3/13/2016
 * Authors:
 *    Colin Riker
 *    Russell Riker
 *    
 * This code will drive two (2) 1 meter RS TriColor RGB LED light stripts to give
 * the Raptor PIT sign some pazza.  Plus drive a couple RED LED's that will be used 
 * to light up the nostrils of the pit sign.
 * 
 */
#include <avr/pgmspace.h>

// ******** DEBUG ==== should auto config to adapt different mother board *********
//#define DATA_1 (PORTF |=  0X01)    // DATA 1    // for ATMEGA
//#define DATA_0 (PORTF &=  0XFE)    // DATA 0    // for ATMEGA
//#define STRIP_PINOUT DDRF=0xFF  // for ATMEGA

#define DATA_1 (PORTC |=  0X01)    // DATA 1    // for UNO
#define DATA_0 (PORTC &=  0XFE)    // DATA 0    // for UNO
#define STRIP_PINOUT (DDRC=0xFF)    // for UNO

int led = 9; 
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by


// the led are RGB with values of 0 to 256 but expressed in hex. ff0000 would be the RED led at full power
// 0xffffff would be the RED, Green, and Blue LED's all at full power which gives you a kind of white, etc..

#define orange 0xff0066 //red at max, and add just a bit of blue
#define red 0xff0000 //red led at max
#define green 0x0000ff //green led at max
#define white 0xffffff //All three at max
#define off 0x000000 //No LED on


PROGMEM const unsigned long candy_cane[10][10]={
  {orange,green,orange,green,orange,green,orange,green,orange,green},
  {green,orange,green,orange,green,orange,green,orange,green,orange}, 
  {orange,green,orange,green,orange,green,orange,green,orange,green},
  {green,orange,green,orange,green,orange,green,orange,green,orange}, 
  {orange,green,orange,green,orange,green,orange,green,orange,green},
  {green,orange,green,orange,green,orange,green,orange,green,orange}, 
  {orange,green,orange,green,orange,green,orange,green,orange,green},
  {green,orange,green,orange,green,orange,green,orange,green,orange}, 
  {orange,green,orange,green,orange,green,orange,green,orange,green},
  {green,orange,green,orange,green,orange,green,orange,green,orange}, 
};

PROGMEM const unsigned long orange_green[10][10]={
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,green},
};

PROGMEM const unsigned long green_orange[10][10]={
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
  {green,green,green,green,green,green,green,green,orange,orange},
};

PROGMEM const unsigned long orange_L2R[10][10]={
  {green,orange,orange,orange,orange,orange,orange,orange,orange,orange},
  {orange,green,orange,orange,orange,orange,orange,orange,orange,orange},
  {orange,orange,green,orange,orange,orange,orange,orange,orange,orange},
  {orange,orange,orange,green,orange,orange,orange,orange,orange,orange},
  {orange,orange,orange,orange,green,orange,orange,orange,orange,orange},
  {orange,orange,orange,orange,orange,green,orange,orange,orange,orange},
  {orange,orange,orange,orange,orange,orange,green,orange,orange,orange},
  {orange,orange,orange,orange,orange,orange,orange,green,orange,orange},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,orange},
  {orange,orange,orange,orange,orange,orange,orange,orange,orange,green},
};

PROGMEM const unsigned long orange_R2L[10][10]={
  {orange,orange,orange,orange,orange,orange,orange,orange,orange,green},
  {orange,orange,orange,orange,orange,orange,orange,orange,green,orange},
  {orange,orange,orange,orange,orange,orange,orange,green,orange,orange},
  {orange,orange,orange,orange,orange,orange,green,orange,orange,orange},
  {orange,orange,orange,orange,orange,green,orange,orange,orange,orange},
  {orange,orange,orange,orange,green,orange,orange,orange,orange,orange},
  {orange,orange,orange,green,orange,orange,orange,orange,orange,orange},
  {orange,orange,green,orange,orange,orange,orange,orange,orange,orange},
  {orange,green,orange,orange,orange,orange,orange,orange,orange,orange},
  {green,orange,orange,orange,orange,orange,orange,orange,orange,orange},
};

PROGMEM const unsigned long pattern_test_orange[10][10]={
  {orange,off,off,off,off,off,off,off,off,off},
  {off,orange,off,off,off,off,off,off,off,off},
  {off,off,orange,off,off,off,off,off,off,off},
  {off,off,off,orange,off,off,off,off,off,off},
  {off,off,off,off,orange,off,off,off,off,off},
  {off,off,off,off,off,orange,off,off,off,off},
  {off,off,off,off,off,off,orange,off,off,off},
  {off,off,off,off,off,off,off,orange,off,off},
  {off,off,off,off,off,off,off,off,orange,off},
  {off,off,off,off,off,off,off,off,off,orange},
};

PROGMEM const unsigned long pattern_test_red[10][10]={
  {0xff0000,off,off,off,off,off,off,off,off,off},
  {off,0xff0000,off,off,off,off,off,off,off,off},
  {off,off,0xff0000,off,off,off,off,off,off,off},
  {off,off,off,0xff0000,off,off,off,off,off,off},
  {off,off,off,off,0xff0000,off,off,off,off,off},
  {off,off,off,off,off,0xff0000,off,off,off,off},
  {off,off,off,off,off,off,0xff0000,off,off,off},
  {off,off,off,off,off,off,off,0xff0000,off,off},
  {off,off,off,off,off,off,off,off,0xff0000,off},
  {off,off,off,off,off,off,off,off,off,0xff0000},
};

PROGMEM const unsigned long pattern_test_blue[10][10]={
  {0x00ff00,off,off,off,off,off,off,off,off,off},
  {off,0x00ff00,off,off,off,off,off,off,off,off},
  {off,off,0x00ff00,off,off,off,off,off,off,off},
  {off,off,off,0x00ff00,off,off,off,off,off,off},
  {off,off,off,off,0x00ff00,off,off,off,off,off},
  {off,off,off,off,off,0x00ff00,off,off,off,off},
  {off,off,off,off,off,off,0x00ff00,off,off,off},
  {off,off,off,off,off,off,off,0x00ff00,off,off},
  {off,off,off,off,off,off,off,off,0x00ff00,off},
  {off,off,off,off,off,off,off,off,off,0x00ff00},
};

PROGMEM const unsigned long pattern_test_green[10][10]={
  {0x0000ff,off,off,off,off,off,off,off,off,off},
  {off,0x0000ff,off,off,off,off,off,off,off,off},
  {off,off,0x0000ff,off,off,off,off,off,off,off},
  {off,off,off,0x0000ff,off,off,off,off,off,off},
  {off,off,off,off,0x0000ff,off,off,off,off,off},
  {off,off,off,off,off,0x0000ff,off,off,off,off},
  {off,off,off,off,off,off,0x0000ff,off,off,off},
  {off,off,off,off,off,off,off,0x0000ff,off,off},
  {off,off,off,off,off,off,off,off,0x0000ff,off},
  {off,off,off,off,off,off,off,off,off,0x0000ff},
};

PROGMEM const unsigned long pattern_test_white[10][10]={
  {0xffffff,off,off,off,off,off,off,off,off,off},
  {off,0xffffff,off,off,off,off,off,off,off,off},
  {off,off,0xffffff,off,off,off,off,off,off,off},
  {off,off,off,0xffffff,off,off,off,off,off,off},
  {off,off,off,off,0xffffff,off,off,off,off,off},
  {off,off,off,off,off,0xffffff,off,off,off,off},
  {off,off,off,off,off,off,0xffffff,off,off,off},
  {off,off,off,off,off,off,off,0xffffff,off,off},
  {off,off,off,off,off,off,off,off,0xffffff,off},
  {off,off,off,off,off,off,off,off,off,0xffffff},
};

PROGMEM const unsigned long pattern_test_comet1[][10]={
  {0xffffff,off,off,off,off,off,off,off,off,off},
  {0x444444,0xffffff,off,off,off,off,off,off,off,off},
  {0x111111,0x444444,0xffffff,off,off,off,off,off,off,off},
  {off,0x111111,0x444444,0xffffff,off,off,off,off,off,off},
  {off,off,0x111111,0x444444,0xffffff,off,off,off,off,off},
  {off,off,off,0x111111,0x444444,0xffffff,off,off,off,off},
  {off,off,off,off,0x111111,0x444444,0xffffff,off,off,off},
  {off,off,off,off,off,0x111111,0x444444,0xffffff,off,off},
  {off,off,off,off,off,off,0x111111,0x444444,0xffffff,off},
  {off,off,off,off,off,off,off,0x111111,0x444444,0xffffff},
};

PROGMEM const unsigned long pattern_test_comet2[][10]={
  {0xffffff,off,off,0x111111,0x444444,0xffffff,off,off,off,off},
  {0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff,off,off,off},
  {0x111111,0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff,off,off},
  {off,0x111111,0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff,off},
  {off,off,0x111111,0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff},
  {0xffffff,off,off,0x111111,0x444444,0xffffff,off,off,off,off},
  {0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff,off,off,off},
  {0x111111,0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff,off,off},
  {off,0x111111,0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff,off},
  {off,off,0x111111,0x444444,0xffffff,off,off,0x111111,0x444444,0xffffff},
};

PROGMEM const unsigned long pattern_test_comet3[][10]={
  {0xffffff,off,off,off,off,off,off,off,off,0xffffff},
  {0x444444,0xffffff,off,off,off,off,off,off,0xffffff,0x444444},
  {0x111111,0x444444,0xffffff,off,off,off,off,0xffffff,0x444444,0x111111},
  {off,0x111111,0x444444,0xffffff,off,off,0xffffff,0x444444,0x111111,off},
  {off,off,0x111111,0x444444,0xffffff,0xffffff,0x444444,0x111111,off,off},
  {off,off,0x111111,0x444444,0xffffff,0xffffff,0x444444,0x111111,off,off},
  {off,off,off,0xffffff,0x444444,0x444444,0xffffff,off,off,off},
  {off,off,0xffffff,0x444444,0x111111,0x111111,0x444444,0xffffff,off,off},
  {off,0xffffff,0x444444,0x111111,off,off,0x111111,0x444444,0xffffff,off},
  {0xffffff,0x444444,0x111111,off,off,off,off,0x111111,0x444444,0xffffff},
};

PROGMEM const unsigned long pattern_test_rainbow[10][10]={
  {0xff0000,0xff7f00,0xffff00,0x00ff00,0x0000ff,0x6f00ff,0x8f00ff,off,off,off},
  {off,0xff0000,0xff7f00,0xffff00,0x00ff00,0x0000ff,0x6f00ff,0x8f00ff,off,off},
  {off,off,0xff0000,0xff7f00,0xffff00,0x00ff00,0x0000ff,0x6f00ff,0x8f00ff,off},
  {off,off,off,0xff0000,0xff7f00,0xffff00,0x00ff00,0x0000ff,0x6f00ff,0x8f00ff},
  {0x8f00ff,off,off,off,0xff0000,0xff7f00,0xffff00,0x00ff00,0x0000ff,0x6f00ff},
  {0x6f00ff,0x8f00ff,off,off,off,0xff0000,0xff7f00,0xffff00,0x00ff00,0x0000ff},
  {0x0000ff,0x6f00ff,0x8f00ff,off,off,off,0xff0000,0xff7f00,0xffff00,0x00ff00},
  {0x00ff00,0x0000ff,0x6f00ff,0x8f00ff,off,off,off,0xff0000,0xff7f00,0xffff00},
  {0xffff00,0x00ff00,0x0000ff,0x6f00ff,0x8f00ff,off,off,off,0xff0000,0xff7f00},
  {0xff7f00,0xffff00,0x00ff00,0x0000ff,0x6f00ff,0x8f00ff,off,off,off,0xff0000},
};




// ***********************************************************************************************************
// *                            Power Up Init.
// ***********************************************************************************************************
void setup() {                

// define some pins to drive a few red led's for the nostrils of the sign


  STRIP_PINOUT;        // set output pin - DEBUG: should auto detect which mother board for use

  reset_strip();
  //noInterrupts();

  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);

}// end setup



// ***********************************************************************************************************
// *
// *                            Main Loop 
// *
// *
// ***********************************************************************************************************
void loop() 
{

  send_1M_pattern(orange_green, 10, 500);
  delay(500);

  send_1M_pattern(green_orange, 10, 500);
  delay(500);

  send_1M_pattern(orange_L2R,10,100);
  delay(100);

  send_1M_pattern(orange_R2L,10,100);
  delay(100);
  
  send_1M_pattern(pattern_test_orange, 10, 100);
  delay(500);


  send_1M_pattern(pattern_test_comet1, 10, 35);
  delay(50);

  send_1M_pattern(pattern_test_comet2, 10, 40);
  delay(50);
  
  send_1M_pattern(pattern_test_comet3, 10, 40);
  delay(50);
  
  // set the brightness of pin 9:
  analogWrite(led, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness == 0 || brightness == 255) {
    fadeAmount = -fadeAmount ;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
  /*
	frame++;
   	if(frame<=10) LEDSTRIP_PATTERN_0();
   	if(10<frame<=20) LEDSTRIP_PATTERN_0();
   	if(20<frame<=30) LEDSTRIP_PATTERN_0();
   	if(frame>30) frame=1;
   */
  


} // *********************** End Main Loop **************************


/*******************************************************************************
 * Function Name  : send_1M_pattern
 * Description    : Transmit pattern to whole 1 meter strip
 *                  
 * Input          : pointer to ROM pattern; pattern length; frame rate
 *                  
 * Output         : None
 * Return         : None
 *******************************************************************************/
void send_1M_pattern(const unsigned long data[][10], int pattern_no, int frame_rate)
{
  int i=0;
  int j=0;
  uint32_t temp_data;

  for (i=0;i<pattern_no;i++)
  {
    noInterrupts();
    for (j=0;j<10;j++)
    {
      temp_data=pgm_read_dword_near(&data[i][j]);
      send_strip(temp_data);
    }
    interrupts();

    delay(frame_rate);

  }




}


/*******************************************************************************
 * Function Name  : send_strip
 * Description    : Transmit 24 pulse to LED strip
 *                  
 * Input          : 24-bit data for the strip
 *                  
 * Output         : None
 * Return         : None
 *******************************************************************************/
void send_strip(uint32_t data)
{
  int i;
  unsigned long j=0x800000;
  
 
  for (i=0;i<24;i++)
  {
    if (data & j)
    {
      DATA_1;
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");    
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      
/*----------------------------*/
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");  
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");  
      __asm__("nop\n\t");  
      __asm__("nop\n\t");        
/*----------------------------*/      
      DATA_0;
    }
    else
    {
      DATA_1;
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");    
      DATA_0;
/*----------------------------*/      
       __asm__("nop\n\t");
      __asm__("nop\n\t");
      __asm__("nop\n\t");      
/*----------------------------*/         
    }

    j>>=1;
  }


  
}

/*******************************************************************************
 * Function Name  : reset_strip
 * Description    : Send reset pulse to reset all color of the strip
 *                  
 * Input          : None
 *                  
 * Output         : None
 * Return         : None
 *******************************************************************************/
void	reset_strip()
{
  DATA_0;
  delayMicroseconds(20);
}
